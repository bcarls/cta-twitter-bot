
import pandas
import sqlite3

con = sqlite3.connect('baseball.db')

cur = con.cursor()    

# Import the schedule data
cur.execute("DROP TABLE IF EXISTS cubs_schedule_2011")
df = pandas.read_csv('baseball/teams_CHC_2011-schedule-scores_team_schedule.csv')
df.to_sql('cubs_schedule_2011', con)

cur.execute("DROP TABLE IF EXISTS cubs_schedule_2012")
df = pandas.read_csv('baseball/teams_CHC_2012-schedule-scores_team_schedule.csv')
df.to_sql('cubs_schedule_2012', con)

cur.execute("DROP TABLE IF EXISTS cubs_schedule_2013")
df = pandas.read_csv('baseball/teams_CHC_2013-schedule-scores_team_schedule.csv')
df.to_sql('cubs_schedule_2013', con)

cur.execute("DROP TABLE IF EXISTS cubs_schedule_2014")
df = pandas.read_csv('baseball/teams_CHC_2014-schedule-scores_team_schedule.csv')
df.to_sql('cubs_schedule_2014', con)

cur.execute("DROP TABLE IF EXISTS cubs_schedule_2015")
df = pandas.read_csv('baseball/teams_CHC_2015-schedule-scores_team_schedule.csv')
df.to_sql('cubs_schedule_2015', con)

cur.execute("DROP TABLE IF EXISTS cubs_schedule_2016")
df = pandas.read_csv('baseball/cubs_schedule_2016.csv')
df.to_sql('cubs_schedule_2016', con)

cur.execute("DROP TABLE IF EXISTS sox_schedule_2011")
df = pandas.read_csv('baseball/teams_CHW_2011-schedule-scores_team_schedule.csv')
df.to_sql('sox_schedule_2011', con)

cur.execute("DROP TABLE IF EXISTS sox_schedule_2012")
df = pandas.read_csv('baseball/teams_CHW_2012-schedule-scores_team_schedule.csv')
df.to_sql('sox_schedule_2012', con)

cur.execute("DROP TABLE IF EXISTS sox_schedule_2013")
df = pandas.read_csv('baseball/teams_CHW_2013-schedule-scores_team_schedule.csv')
df.to_sql('sox_schedule_2013', con)

cur.execute("DROP TABLE IF EXISTS sox_schedule_2014")
df = pandas.read_csv('baseball/teams_CHW_2014-schedule-scores_team_schedule.csv')
df.to_sql('sox_schedule_2014', con)

cur.execute("DROP TABLE IF EXISTS sox_schedule_2015")
df = pandas.read_csv('baseball/teams_CHW_2015-schedule-scores_team_schedule.csv')
df.to_sql('sox_schedule_2015', con)

cur.execute("DROP TABLE IF EXISTS sox_schedule_2016")
df = pandas.read_csv('baseball/sox_schedule_2016.csv')
df.to_sql('sox_schedule_2016', con)

cur.execute("DROP TABLE IF EXISTS wrigley_events")
df = pandas.read_csv('baseball/wrigley_events.csv')
df.to_sql('wrigley_events', con)



