
import pandas
import sqlite3

con = sqlite3.connect('gtfs_cta.db')

cur = con.cursor()    

# Import the trips.txt data
cur.execute("DROP TABLE IF EXISTS trips")
df = pandas.read_csv('gtfs/trips.txt')
df[(df.route_id == 'P') | (df.route_id == 'Y') | (df.route_id == 'Red') | (df.route_id == 'P') | (df.route_id == 'Pink') | (df.route_id == 'G') | (df.route_id == 'Blue') | (df.route_id == 'Brn')].to_sql('trips', con)

# Import the stop_times.txt data
cur.execute("DROP TABLE IF EXISTS stop_times")
df = pandas.read_csv('gtfs/stop_times.txt')
df.to_sql('stop_times', con)
cur.execute("DELETE FROM stop_times WHERE trip_id NOT IN (SELECT trip_id FROM trips)")

# Import the stops.txt data
cur.execute("DROP TABLE IF EXISTS stops")
df = pandas.read_csv('gtfs/stops.txt')
df.to_sql('stops', con)
cur.execute("DELETE FROM stops WHERE stop_id <= 30000")

# Import the calendar.txt data
cur.execute("DROP TABLE IF EXISTS calendar")
df = pandas.read_csv('gtfs/calendar.txt')
df.to_sql('calendar', con)


