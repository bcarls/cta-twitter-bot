
import cta_arrival
import bot_utils
from datetime import datetime, date, time
import time
import pytz
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.dates import DateFormatter
import pandas


delays = cta_arrival.analyze_recorded_arrival_data()

blue_status = bot_utils.train_line_status('Blue')
red_status  = bot_utils.train_line_status('Red')
green_status  = bot_utils.train_line_status('Green')
brown_status  = bot_utils.train_line_status('Brown')
pink_status  = bot_utils.train_line_status('Pink')
yellow_status  = bot_utils.train_line_status('Yellow')
orange_status  = bot_utils.train_line_status('Orange')
purple_status  = bot_utils.train_line_status('Purple')

train_statuses = dict([('Blue',blue_status),
        ('Red',red_status),
        ('Green',green_status),
        ('Brown',brown_status),
        ('Pink',pink_status),
        ('Yellow',yellow_status),
        ('Orange',orange_status),
        ('Purple',purple_status)])



# Analyze the delays and look for type 0
print 'Analyzing the delays found.'
route_name_0 = 'None'
stop_name_0 = 'None'
destNm_0 = 'None'
delay_minutes_0 = 'None'
n_delays = 0
for delay in delays:
    train_statuses[delay.route_name].add_station(delay.delay_minutes,delay.destNm,delay.dt_prdt)

for train_statuses_key, train_statuses_value in train_statuses.iteritems():
    for destinations_key, destinations_value  in train_statuses_value.destinations.iteritems():

        df = pandas.DataFrame({'dt_prdt':train_statuses[train_statuses_key].destinations[destinations_key].dt_prdt,'delay_minutes':train_statuses[train_statuses_key].destinations[destinations_key].delay_minutes})

        print df

        df.index = df.dt_prdt
        plot = df.delay_minutes.plot()
        fig = plot.get_figure()
        fig.savefig(destinations_key.replace('\'','').replace('/','').replace('\ ','') + '.png')

        # fig, ax = plt.subplots()
        # # plt.figure()
        # dates = matplotlib.dates.date2num(train_statuses[train_statuses_key].destinations[destinations_key].dt_prdt)
        # plt.plot_date(dates, train_statuses[train_statuses_key].destinations[destinations_key].delay_minutes)
        # # plt.show()
        # ax.fmt_xdata = DateFormatter('%Y-%m-%d') 
        # fig.autofmt_xdate()
        # plt.savefig(destinations_key.replace('\'','').replace('/','').replace('\ ','') + '.png')


