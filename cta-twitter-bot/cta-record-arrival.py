
import cta_arrival
import cta_status
from datetime import datetime, date, time
import time
import pytz

# Our date and time
dt = datetime.now(pytz.timezone('US/Central'))

# Runs every five minutes for an hour
counter = 0
while counter < 12*24:
    # Let's get data
    print 'Grabbing data, counter: ' + str(counter)
    cta_arrival.record_arrival_data(dt)
    cta_status.record_status_data(dt)
    time.sleep(60*5)
    counter = counter + 1

