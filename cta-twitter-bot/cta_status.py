
import bot_utils
import urllib
import sqlite3
from datetime import datetime, date, time
import time
import calendar
from xml.etree import ElementTree as ET
import requests

def record_status_data(dt):

    # Now we go use the Route Status API
    queryURLStatus = 'http://www.transitchicago.com/api/1.0/routes.aspx'

    # This is the API call
    s = requests.get(queryURLStatus ) 
    root = ET.fromstring(s.content)

    # Write it to file 
    with open('recorded_xml/status_' + dt.strftime('%Y-%m-%d_%H:%M:%S') + '.xml', "a") as myfile:
        myfile.write(ET.tostring(root,encoding='utf-8').decode('utf-8')+'\n')

