
import tweepy
import requests
from keys import twitter_keys, cta_key
import bot_utils
import sqlite3
from datetime import datetime, date, time
import pytz
import time
import calendar
from xml.etree import ElementTree as ET
import glob

class type_0_data():

    def set_data(self, route_name, stop_name, destNm, delay_minutes):
        self.route_name = route_name
        self.destNm = destNm
        self.delay_minutes = delay_minutes 
        self.stop_name = stop_name

    def set_datetime(self, datetime):
        self.datetime = datetime

    def table_fill_command():
        return "INSERT INTO type_0 (datetime, route_name, stop_name, destNm, delay_minutes) VALUES (?,?,?,?,?);".format(self.datetime,self.route_name,self.stop_name, self.destNm,self.delay_minutes)



class type_1_data():

    def __init__(self, datetime, route_name, destNm, delay_minutes):
        self.datetime = datetime
        self.route_name = route_name
        self.destNm = destNm
        self.delay_minutes = delay_minutes 

    def table_fill_command(self):
        return 'INSERT INTO type_1 (datetime, route_name, destNm, delay_minutes) VALUES (\"{0}\",\"{1}\",\"{2}\",{3});'.format(self.datetime,self.route_name, self.destNm,self.delay_minutes)


class type_2_data():

    def __init__(self, datetime, team_name):
        self.datetime = datetime
        self.team_name = team_name

    def table_fill_command(self):
        return 'INSERT INTO type_2 (datetime, team_name) VALUES (\"{0}\",\"{1}\");'.format(self.datetime,self.team_name)


class type_3_data():

    def __init__(self, datetime, team_name, stadium_name):
        self.datetime = datetime
        self.team_name = team_name
        self.stadium = stadium_name

    def table_fill_command(self):
        return 'INSERT INTO type_3 (datetime, team_name, stadium) VALUES (\"{0}\",\"{1}\",\"{2}\");'.format(self.datetime,self.team_name,self.stadium)

class type_4_data():

    def __init__(self, datetime, route_name, route_status):
        self.datetime = datetime
        self.route_name = route_name
        self.route_status = route_status

    def table_fill_command(self):
        return 'INSERT INTO type_4 (datetime, route_name, route_status) VALUES (\"{0}\",\"{1}\",\"{2}\");'.format(self.datetime,self.route_name, self.route_status)

class type_5_data():

    def __init__(self, datetime, delay_minutes):
        self.datetime = datetime
        self.delay_minutes = delay_minutes

    def table_fill_command(self):
        return 'INSERT INTO type_5 (datetime, delay_minutes) VALUES (\"{0}\",\"{1}\");'.format(self.datetime,self.delay_minutes)


def calc_time_since_last_tweet(dt_tweet, dt_last_tweet):
    return int(abs(time.mktime(dt_tweet.timetuple()) - time.mktime(dt_last_tweet.timetuple()))) / 60


def post_tweet(to_tweet):

    consumer_key = twitter_keys['consumer_key']
    consumer_secret = twitter_keys['consumer_secret']
    access_token = twitter_keys['access_token']
    access_token_secret = twitter_keys['access_token_secret']

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    
    api = tweepy.API(auth)
    
    try:
        return api.update_status(to_tweet + ' #CTA')
    except:
        print('Failed to post tweet, might be a duplicate.')


class arrival_delay:

    def __init__(self, stopID, stationName, destName, routeID, runNumber, delayMinutes, dt_prdt):
        self.stop_id = stopID
        self.stop_name = stationName
        self.destNm = destName
        self.route_id = routeID
        self.route_name = routeID
        if routeID == "Brn":
            self.route_name = "Brown"
        if routeID == "G":
            self.route_name = "Green"
        if routeID == "Y":
            self.route_name = "Yellow"
        if routeID == "Org":
            self.route_name = "Orange"
        if routeID == "P":
            self.route_name = "Purple"
        self.rn = runNumber
        self.delay_minutes = delayMinutes
        self.dt_prdt = dt_prdt

class destination:

    

    def __init__(self, destName, delay_minutes, dt_prdt):
        self.dt_prdt = []
        self.delay_minutes = []
        self.dest_name = destName
        self.n_query_results = 1
        self.delay_minutes_total = delay_minutes
        self.delay_minutes.append(delay_minutes)
        self.dt_prdt.append(dt_prdt)

    def get_average_delay():
        return self.delay_minutes_total/self.n_query_results


class train_line_status:

    def __init__(self, routeID):
        self.route_id = routeID
        self.destinations = dict()

        # This is the API call to get overall statuses per line 
        route_id_query = routeID
        if routeID == "Brown":
            route_id_query = "Brn"
        if routeID == "Green":
            route_id_query = "G"
        if routeID == "Yellow":
            route_id_query = "Y"
        if routeID == "Orange":
            route_id_query = "Org"
        if routeID == "Purple":
            route_id_query = "P"
        query_url_status = 'http://www.transitchicago.com/api/1.0/routes.aspx'
        url = query_url_status + '?routeid=' + route_id_query 
        s = requests.get(url) 
        root = ET.fromstring(s.content)

        self.route_status = ''
        items = root.findall('RouteInfo')
        for item in items:
            self.route_status = item.find('RouteStatus').text

    def add_station(self, delay_minutes, dest_name, dt_prdt):
        if dest_name not in self.destinations:
            self.destinations[dest_name] = destination(dest_name, delay_minutes, dt_prdt);
        else:
            self.destinations[dest_name].n_query_results = self.destinations[dest_name].n_query_results + 1
            self.destinations[dest_name].delay_minutes_total = self.destinations[dest_name].delay_minutes_total + delay_minutes
            self.destinations[dest_name].dt_prdt.append(dt_prdt)
            self.destinations[dest_name].delay_minutes.append(delay_minutes)

    def get_average_delay(self,dest_name):
        return self.destinations[dest_name].delay_minutes_total/self.destinations[dest_name].n_query_results

    def clear(self):
        self.destinations.clear()


class baseball_game: 

    def __init__(self, team, datetime, stadium):
        self.team = team
        self.datetime = datetime
        self.stadium = stadium


def find_delays(xml_directory, dt, record_data):

    if xml_directory == '':
        print(str(datetime.now()) + ' -- ' + 'No xml directory specified, using live data.')
    else: 
        print(str(datetime.now()) + ' -- ' + 'Using the xml directory ' + xml_directory + ' for CTA status.')

    # Connect to the database to get static info
    con = sqlite3.connect('gtfs_cta.db')
    cur = con.cursor()    
    
    # Select the train stops from the stops database 
    # cur.execute("SELECT * FROM stops WHERE stop_id > 30000 AND stop_id < 30013")
    # cur.execute("SELECT * FROM stops WHERE stop_id == 30001")
    cur.execute("SELECT * FROM stops WHERE stop_id > 30000")
    rows = cur.fetchall()
    
    # Holding all of our delays
    delays = []
   
    # Now we go use the Arrival API and get station statuses
    query_url_arrivals = 'http://lapi.transitchicago.com/api/1.0/ttarrivals.aspx?key=' + cta_key
    # Loop over stops (stations)
    i_stations = 0 
    for row in rows:
        i_stations = i_stations + 1

        if xml_directory == '':

            # This is the API call
            url = query_url_arrivals + '&stpid=' + str(row[1]) + '&max=2'
            s = requests.get(url) 
            root = ET.fromstring(s.content)

            if record_data == True:
                root_record = root 
                with open('recorded_xml/' + str(row[1])+ '_' + dt.strftime('%Y-%m-%d_%H:%M:%S') + '.xml', "a") as myfile:
                    myfile.write(ET.tostring(root_record,encoding='utf-8').decode('utf-8')+'\n')
        else:
            # This uses xml files already created for testing
            tree = ET.parse(xml_directory + '/' + str(row[1]) + '.xml')
            root = tree.getroot()

        
        # Loop through all eta's for the stop
        items = root.findall('eta')
        for item in items:
            dtarrT = datetime.strptime(item.find('arrT').text, "%Y%m%d %H:%M:%S")
            cur.execute('SELECT * FROM stop_times WHERE trip_id IN (SELECT trip_id FROM trips WHERE schd_trip_id == \'R' + item.find('rn').text + '\' AND service_id IN (SELECT service_id FROM calendar WHERE ' + calendar.day_name[dtarrT.weekday()] +' ==1)) AND stop_id == ' + item.find('stpId').text)
            stop_times_rows = cur.fetchall()

            # Now we have to check all stop times for the run 
            # We are looking for delays
            min_time = 60*24
            for stop_time_row in stop_times_rows:
                try:
                    dtschdT = datetime.strptime( str(dtarrT.year) + str(dtarrT.strftime('%m')) + str(dtarrT.strftime('%d')) + " " + stop_time_row[2], "%Y%m%d %H:%M:%S")
                    min_time = min(min_time,int(abs(time.mktime(dtschdT.timetuple()) - time.mktime(dtarrT.timetuple()))) / 60)
                except ValueError:
                    continue

            # If the delay time is greater than 60 minutes, it's not matched to a schedule, move on
            if min_time > 60:
                continue
            delays.append(bot_utils.arrival_delay(item.find('stpId').text, 
                item.find('staNm').text,
                item.find('destNm').text, 
                item.find('rt').text, 
                item.find('rn').text,
                min_time,
                dtarrT))
        if i_stations%100 == 0:
            print(str(datetime.now()) + ' -- ' + 'Looked at ' + str(i_stations) + ' stops so far.')

    return delays




def analyze_recorded_arrival_data():

    # Connect to the database to get static info
    con = sqlite3.connect('gtfs_cta.db')
    cur = con.cursor()    

    # Holding all of our delays
    delays = []

    # Hold the amount of the delay and the datetime for the predicted
    dt_predicted = []
    delay_min = []


    for file_glob in glob.glob('recorded_xml/30001*_*.xml'):
    # for file_glob in glob.glob('recorded_xml/30001_2016-06-08_22:05:01.xml'):
        print('Opening ' + file_glob)
        with open(file_glob) as f:
            for line in f:
                # print line 
                root = ET.fromstring(line)

                items = root.findall('eta')
                for item in items:
                    dtarrT = datetime.strptime(item.find('arrT').text, "%Y%m%d %H:%M:%S")
                    cur.execute('SELECT trip_id,arrival_time FROM stop_times WHERE stop_id == ' + item.find('stpId').text + ' AND trip_id IN (SELECT trip_id FROM trips WHERE schd_trip_id == \'R' + item.find('rn').text + '\' AND service_id IN (SELECT service_id FROM calendar WHERE ' + calendar.day_name[dtarrT.weekday()] +' == 1))')

                    stop_times_rows = cur.fetchall()

                    # Now we have to check all stop times for the run 
                    # We are looking for delays
                    min_time = 60*24
                    for stop_time_row in stop_times_rows:
                        try:
                            dtschdT = datetime.strptime( str(dtarrT.year) + str(dtarrT.strftime('%m')) + str(dtarrT.strftime('%d')) + " " + stop_time_row[1], "%Y%m%d %H:%M:%S")
                            min_time = min(min_time,int(abs(time.mktime(dtschdT.timetuple()) - time.mktime(dtarrT.timetuple()))) / 60)
                        except ValueError:
                            continue

                    # If the delay time is greater than 60 minutes, it's not matched to a schedule, move on
                    if min_time > 60:
                        continue

                    dt_prdt = datetime.strptime(item.find('prdt').text, "%Y%m%d %H:%M:%S")

                    dt_predicted.append(dt_prdt)
                    delay_min.append(min_time)

                    delays.append(bot_utils.arrival_delay(item.find('stpId').text, 
                        item.find('staNm').text,
                        item.find('destNm').text, 
                        item.find('rt').text, 
                        item.find('rn').text,
                        min_time,
                        dt_prdt))
    return delays










