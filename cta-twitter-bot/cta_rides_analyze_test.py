
import pandas
import baseball
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import r2_score
import pandas as pd
import numpy as np
import pylab as pl
from datetime import datetime
import pickle

# cta_l_riders = pandas.read_csv("CTA_-_Ridership_-__L__Station_Entries_-_Daily_Totals.csv")

# # Addison Red line
# station = cta_l_riders[(cta_l_riders.station_id==41420)]
# # Addison Brown line
# # station = cta_l_riders[(cta_l_riders.station_id==41440)]
# # Sox-35th
# # station = cta_l_riders[(cta_l_riders.station_id==40190)]

# station.date = station.date.apply(lambda d: datetime.strptime(d, "%m/%d/%Y"))

# cta_l_riders.index = cta_l_riders.date

# station.insert(len(station.columns),'day_of_year', station['date'])
# station.day_of_year = station.day_of_year.apply(lambda d: d.strftime('%j'))

# station.insert(len(station.columns),'cubs_game', station['date'])
# station.cubs_game = station.cubs_game.apply(lambda d: baseball.find_baseball_game('cubs',d))

# station.insert(len(station.columns),'day_of_week', station['date'])
# station.day_of_week = station.day_of_week.apply(lambda d: d.strftime('%w'))

# station.daytype = station.daytype.apply(lambda d: 0 if d == "W" else d)
# station.daytype = station.daytype.apply(lambda d: 1 if d == "A" else d)
# station.daytype = station.daytype.apply(lambda d: 2 if d == "U" else d)
# station.cubs_game = station.cubs_game.apply(lambda d: 0 if d == False else d)
# station.cubs_game = station.cubs_game.apply(lambda d: 1 if d == True else d)
# # station[station.date > datetime.strptime("01/01/2011","%m/%d/%Y") ].rides.plot()

# # features = ['daytype','day_of_year','cubs_game']
# features = ['day_of_year','day_of_week','cubs_game']
# # features = ['day_of_year','day_of_week']
# # features = ['day_of_year','cubs_game']
# # features = ['daytype','day_of_year']

# train, test = station[(station.date > datetime.strptime("01/01/2011","%m/%d/%Y")) & (station.date < datetime.strptime("01/01/2014","%m/%d/%Y")) ], station[station.date >=datetime.strptime("01/01/2014","%m/%d/%Y")]


rf = pickle.load(open('test.pkl','rb'))

# print(rf.predict(test[features]))


print(rf.predict(np.array([46,1,0]).reshape(1, -1)))

