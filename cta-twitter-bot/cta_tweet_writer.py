
from datetime import datetime, date, time
import time
import pytz
import sqlite3
import bot_utils
import cta_status
import baseball
import numpy
import tweepy
import pickle
import sklearn

class PeriodicTweets():


    def __init__(self,train_statuses,corpus):
        self.deactivate = False
        self.train_statuses = train_statuses
        self.corpus = corpus

    def run(self, xml_directory, record_data, post_tweet):

        # Record the time 
        dt_record = datetime.now(pytz.timezone('US/Central'))
        
        
        # Times to wait before Tweeting about the same topic in minutes, times are in minutes
        type_0_wait = 60
        type_1_wait = 60
        type_2_wait = 60*12
        type_3_wait = 60*12
        type_4_wait = 60*2
        type_5_wait = 60*3

        # Total number of cycles, 1 cycle is 60*5 seconds
        n_cycles = 12*24

        # Time to sleep between recording data in seconds
        sleep_length_record = 5*60

        # Time to sleep between tweets in seconds
        sleep_length_tweet = 30*60

        # The minimum delay to trigger a tweet
        min_delay_minutes = 5


        # Connect to the database for storing already made Tweets
        con = sqlite3.connect('tweets_made.db')
        cur = con.cursor()    
        cur.execute("CREATE TABLE IF NOT EXISTS type_0 (datetime TEXT, route_name TEXT, stop_name TEXT, destNm TEXT, delay_minutes INT)")
        cur.execute("CREATE TABLE IF NOT EXISTS type_1 (datetime TEXT, route_name TEXT, destNm TEXT, delay_minutes INT)")
        cur.execute("CREATE TABLE IF NOT EXISTS type_2 (datetime TEXT, team_name TEXT)")
        cur.execute("CREATE TABLE IF NOT EXISTS type_3 (datetime TEXT, team_name TEXT, stadium TEXT)")
        cur.execute("CREATE TABLE IF NOT EXISTS type_4 (datetime TEXT, route_name TEXT, route_status TEXT)")
        cur.execute("CREATE TABLE IF NOT EXISTS type_5 (datetime TEXT, delay_minutes INT)")

        # Runs every five minutes for 24 hours
        counter = 0
        while counter < n_cycles:

            # Keeps track if it's a tweeting cycle, it gets set to true in the tweeting cycle if statement
            new_tweet_cycle = False

            print('\n')
            print('-------------------------------------')
            print(' ' + str(datetime.now()))
            print(' New Data Taking Cycle, count ' + str(counter))
            print('-------------------------------------')
            print('\n')

            # Reset the tweet type
            tweet_type = -1

            # Record the time when a tweet might be made
            dt_tweet = datetime.now(pytz.timezone('US/Central'))

            # Dictionaries to hold possible Tweets and their data
            potential_tweets = dict()
            potential_tweets_data = dict()

            # Let's get delays
            print(str(datetime.now()) + ' -- ' + 'Looking for train delays stop by stop.')
            delays = []
            delays = bot_utils.find_delays(xml_directory, dt_record, record_data)
            cta_status.record_status_data(dt_record)

            # Analyze the delays and look for type 0
            print(str(datetime.now()) + ' -- ' + 'Analyzing the delays found.')
            station_delay_print = ''
            type_0_data = bot_utils.type_0_data() 

            n_delays = 0
            # We start by clearing out the old statuses
            for train_statuses_key, train_statuses_value in self.train_statuses.items():
                train_statuses_value.clear()

            for delay in delays:
                self.train_statuses[delay.route_name].add_station(delay.delay_minutes,delay.destNm,delay.dt_prdt)

                if delay.delay_minutes > 1:
                    n_delays = n_delays + 1

                station_delay_print = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==0].index,1)].iloc[0][1].replace("[destNm]",delay.destNm).replace('[route_name]',delay.route_name).replace('[stop_name]',delay.stop_name).replace("[delay_minutes]",str(delay.delay_minutes))
                
                self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==0].index,1)].iloc[0][1].replace("[destNm]",delay.destNm).replace('[route_name]',delay.route_name).replace('[stop_name]',delay.stop_name).replace("[delay_minutes]",str(delay.delay_minutes))
                if delay.delay_minutes > 1:
                    station_delay_print = station_delay_print.replace("[minute_s]","minutes")
                else:
                    station_delay_print = station_delay_print.replace("[minute_s]","minute")

                type_0_data.set_data(delay.route_name, delay.stop_name, delay.destNm, delay.delay_minutes)

            print(str(datetime.now()) + ' -- ' + 'Looked at ' + str(len(delays)) + ' delays total.')
            print(str(datetime.now()) + ' -- ' + 'Found ' + str(n_delays) + ' per station delays of greater than 1 minute.')
            type_0_data.set_datetime(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'))
            potential_tweets[0] = station_delay_print
            potential_tweets_data[0] = type_0_data
       




            # Go through the train lines and look for significant delays along an entire line
            # Look for type 1
            print(str(datetime.now()) + ' -- ' + 'Going through the train lines and looking for significant delays along an entire line.')
            max_train_delay = 0
            max_train_delay_line = ''
            max_train_delay_destNm = ''
            max_train_delay_tweet = ''
            for train_statuses_key, train_statuses_value in self.train_statuses.items():
                for destinations_key, destinations_value  in train_statuses_value.destinations.items():
                    to_print = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==1].index,1)].iloc[0][1].replace("[destNm]",destinations_key).replace('[route_name]',train_statuses_key).replace("[delay_minutes]",str(int(train_statuses_value.get_average_delay(destinations_key))))
                    if train_statuses_value.get_average_delay(destinations_key) > 1:
                        to_print = to_print.replace("[minute_s]","minutes")
                    else:
                        to_print = to_print.replace("[minute_s]","minute")

                    # Check if this is the largest delay and that it hasn't been tweeted about in the last hour already

                    if train_statuses_value.get_average_delay(destinations_key) > max_train_delay:

                        cur.execute('SELECT datetime FROM type_1 WHERE route_name == \"' + train_statuses_key + '\" AND destNm == \"' + destinations_key + '\"')
                        rows = cur.fetchall()
                        # Time since last tweet in minutes
                        time_since_last_tweet = type_1_wait 
                        n_rows = 0
                        for row in rows:
                            n_rows = n_rows + 1
                            dt_last_tweet = datetime.strptime(row[0], "%Y-%m-%d_%H:%M:%S")
                            time_since_last_tweet = min(time_since_last_tweet,int(abs(time.mktime(dt_tweet.timetuple()) - time.mktime(dt_last_tweet.timetuple()))) / 60)
                            time_since_last_tweet = min(time_since_last_tweet, 
                            bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                                , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                        if time_since_last_tweet < type_1_wait or n_rows < 1:
                            max_train_delay_line = train_statuses_key
                            max_train_delay_destNm = destinations_key
                            max_train_delay = train_statuses_value.get_average_delay(destinations_key)
                            max_train_delay_tweet = to_print 
            
            if max_train_delay >= min_delay_minutes:
                print(str(datetime.now()) + ' -- ' + 'Found a delay of ' + str(max_train_delay) + ' minutes on the ' + max_train_delay_line + ' headed to ' + max_train_delay_destNm + '.')
                potential_tweets[1] = max_train_delay_tweet
                potential_tweets_data[1] = bot_utils.type_1_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'),max_train_delay_line, max_train_delay_destNm, int(max_train_delay))
            else:
                print(str(datetime.now()) + ' -- ' + 'Found no delays over ' + str(min_delay_minutes) + '.')
                if max_train_delay != 0:
                    print(str(datetime.now()) + ' -- ' + 'Largest found was ' + str(max_train_delay) + ' on the ' + max_train_delay_line + ' headed to ' + max_train_delay_destNm + '.')
                potential_tweets[5] = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==5].index,1)].iloc[0][1].replace("[delay_minutes]",str(min_delay_minutes))
                potential_tweets_data[5] = bot_utils.type_5_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), min_delay_minutes)





            # Let's look for abnormal statuses, e.g. queries like http://www.transitchicago.com/api/1.0/routes.aspx?routeid=y
            n_normal_status = 0
            for train_statuses_key, train_statuses_value in self.train_statuses.items():

                # Check if all lines have normal status
                if "Normal Service" in train_statuses_value.route_status:
                    n_normal_status = n_normal_status + 1

            if n_normal_status == 8:
                potential_tweets[4] = 'All train lines are operating with Normal Service.'
                potential_tweets_data[4] = bot_utils.type_4_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "All", "Normal Service")






            # Has the twitter sleep amount passed? If so, tweet. 
            if (sleep_length_record*counter)%sleep_length_tweet == 0: 
                new_tweet_cycle = True
                print('\n')
                print('-------------------------------------')
                print(str(datetime.now()) + ' -- ' + ' New Full Tweeting Cycle')
                print('-------------------------------------')
                print('\n')


                # Let's look for games
                print(str(datetime.now()) + ' -- ' + 'Looking for games.')
                baseball_games = baseball.find_baseball_games()
                found_sox = False
                found_cubs = False
                found_sox_started = False
                found_cubs_started = False
                baseball_tweet_sox = ''
                baseball_tweet_cubs = ''
                baseball_tweet_sox_started = ''
                baseball_tweet_cubs_started  = ''
                for baseball_game in baseball_games:
                    if baseball_game.datetime > dt_tweet.replace(tzinfo=None):
                        if baseball_game.team == 'sox':
                            baseball_tweet_sox = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==2].index,1)].iloc[0][1].replace("[team_name]","Sox").replace("[stadium]","U.S. Cellular").replace("[stop_name]","Sox-35th").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                            found_sox = True
                        if baseball_game.team == 'cubs':
                            baseball_tweet_cubs = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==2].index,1)].iloc[0][1].replace("[team_name]","Cubs").replace("[stadium]","Wrigley").replace("[stop_name]","Addison").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                            found_cubs = True
                    else:
                        if baseball_game.team == 'sox':
                            baseball_tweet_sox_started  = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==3].index,1)].iloc[0][1].replace("[team_name]","Sox").replace("[stadium]","U.S. Cellular").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                            found_sox_started = True
                        if baseball_game.team == 'cubs':
                            baseball_tweet_cubs_started = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==3].index,1)].iloc[0][1].replace("[team_name]","Cubs").replace("[stadium]","Wrigley").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                            found_cubs_started = True


                # Is the Cubs game going to have a significant number of people at the Addison red line stop?
                # The array goes day of the year, day of the week, and T/F for cubs game
                rf = pickle.load(open('addison_red_ride_model.pkl','rb'))
              
                if found_cubs:
                    cubs_predict = rf.predict(numpy.array([dt_tweet.strftime('%j'),dt_tweet.strftime('%j'),1]).reshape(1, -1))
                    cubs_predict_nogame = rf.predict(numpy.array([dt_tweet.strftime('%j'),dt_tweet.strftime('%j'),0]).reshape(1, -1))
                    # print(cubs_predict[0])
                    if cubs_predict > 14000:
                        baseball_tweet_cubs = baseball_tweet_cubs + ' ' + str(int(round(cubs_predict[0],-2))) + ' people expected at Addison stop today vs '+ str(int(round(cubs_predict_nogame[0],-2))) +  ' no game.'
                        baseball_tweet_cubs_started = baseball_tweet_cubs + ' ' + str(int(round(cubs_predict[0],-2))) + ' people expected at Addison stop today vs '+ str(int(round(cubs_predict_nogame[0],-2))) +  ' no game.'

                if found_cubs and found_sox:
                    potential_tweets[2] = baseball_tweet_cubs
                    potential_tweets_data[2] = bot_utils.type_2_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "Cubs")
                elif found_cubs:
                    potential_tweets[2] = baseball_tweet_cubs
                    potential_tweets_data[2] = bot_utils.type_2_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "Cubs")
                elif found_sox:
                    potential_tweets[2] = baseball_tweet_sox
                    potential_tweets_data[2] = bot_utils.type_2_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "Sox")

                if found_cubs_started and found_sox_started:
                    potential_tweets[3] = baseball_tweet_cubs_started
                    potential_tweets_data[3] = bot_utils.type_3_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "Cubs", "Wrigley")
                elif found_cubs_started:
                    potential_tweets[3] = baseball_tweet_cubs_started
                    potential_tweets_data[3] = bot_utils.type_3_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "Cubs", "Wrigley")
                elif found_sox_started:
                    potential_tweets[3] = baseball_tweet_sox_started
                    potential_tweets_data[3] = bot_utils.type_3_data(dt_tweet.strftime('%Y-%m-%d_%H:%M:%S'), "Sox", "U.S. Cellular")






                
                print('\n')
                print(str(datetime.now()) + ' -- ' + 'Potential tweets: ')
                for tweet in potential_tweets:
                    print(potential_tweets[tweet])
                # Is there a train delay over 5 minutes that hasn't been posted in the last hour?
                if 1 in potential_tweets_data:
                    # Time since last tweet in minutes
                    time_since_last_tweet = type_1_wait
                    cur.execute('SELECT datetime FROM type_1 WHERE route_name == \"' + potential_tweets_data[1].route_name + '\" AND destNm == \"' + potential_tweets_data[1].destNm + '\"')
                    rows = cur.fetchall()
                    n_rows = 0
                    for row in rows:
                        # print row[0]
                        n_rows = n_rows + 1
                        time_since_last_tweet = min(time_since_last_tweet, 
                        bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                            , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                    print(str(datetime.now()) + ' -- ' + 'Time since last type 1 tweet: ' + str(time_since_last_tweet) + ' minutes')
                    # If it's been longer than 60 minutes since the last tweet about this, tweet it again
                    if time_since_last_tweet >= type_1_wait or n_rows < 1:
                        tweet_type = 1

                # Check if there's a baseball game and tweet about it
                if tweet_type < 0:
                    # Is it before the game started?
                    if 2 in potential_tweets_data:
                        # Time since last tweet in minutes
                        time_since_last_tweet = type_2_wait
                        cur.execute('SELECT datetime FROM type_2')
                        rows = cur.fetchall()
                        for row in rows:
                            time_since_last_tweet = min(time_since_last_tweet, 
                            bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                                , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                        print(str(datetime.now()) + ' -- ' + 'Time since last type 2 tweet: ' + str(time_since_last_tweet) + ' minutes')
                        if time_since_last_tweet >= type_2_wait:
                            tweet_type = 2
                    # Is it after the game started?
                    if 3 in potential_tweets_data:
                        # Time since last tweet in minutes
                        time_since_last_tweet = type_3_wait
                        cur.execute('SELECT datetime FROM type_3')
                        rows = cur.fetchall()
                        for row in rows:
                            time_since_last_tweet = min(time_since_last_tweet, 
                            bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                                , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                        print('Time since last type 3 tweet: ' + str(time_since_last_tweet) + ' minutes')
                        if time_since_last_tweet >= type_3_wait:
                            tweet_type = 3


                # Do we tweet that all service is normal?
                if tweet_type < 0:
                    if 4 in potential_tweets_data:
                        time_since_last_tweet = type_4_wait
                        cur.execute('SELECT datetime FROM type_4 WHERE route_name == \"' + potential_tweets_data[4].route_name + '\" AND route_status == \"' + potential_tweets_data[4].route_status + '\"')
                        rows = cur.fetchall()
                        n_rows = 0
                        for row in rows:
                            n_rows = n_rows + 1
                            time_since_last_tweet = min(time_since_last_tweet, 
                            bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                                , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                        print(str(datetime.now()) + ' -- ' + 'Time since last type 4 tweet: ' + str(time_since_last_tweet) + ' minutes')
                        if time_since_last_tweet >= type_4_wait:
                            tweet_type = 4

                # Do we tweet that no delays were found along the lines?
                if tweet_type < 0:
                    if 5 in potential_tweets_data:
                        # Time since last tweet in minutes
                        time_since_last_tweet = type_5_wait
                        cur.execute('SELECT datetime FROM type_5')
                        rows = cur.fetchall()
                        n_rows = 0
                        for row in rows:
                            n_rows = n_rows + 1
                            time_since_last_tweet = min(time_since_last_tweet, 
                            bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                                , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                        print(str(datetime.now()) + ' -- ' + 'Time since last type 5 tweet: ' + str(time_since_last_tweet) + ' minutes')
                        # If it's been longer than 60 minutes since the last tweet about this, tweet it again
                        if time_since_last_tweet >= type_1_wait or n_rows < 1:
                            tweet_type = 5







            # If a new delay has appeared within the last five minutes, look to tweet it 
            if not new_tweet_cycle:
                print('\n')
                print(str(datetime.now()) + ' -- ' + 'Potential out of normal cycle tweets: ')
                for tweet in potential_tweets:
                    print(potential_tweets[tweet])
                # Is there a train delay over 5 minutes that hasn't been posted in the last hour?
                if 1 in potential_tweets_data:
                    # Time since last tweet in minutes
                    time_since_last_tweet = type_1_wait
                    cur.execute('SELECT datetime FROM type_1 WHERE route_name == \"' + potential_tweets_data[1].route_name + '\" AND destNm == \"' + potential_tweets_data[1].destNm + '\"')
                    rows = cur.fetchall()
                    n_rows = 0
                    for row in rows:
                        n_rows = n_rows + 1
                        time_since_last_tweet = min(time_since_last_tweet, 
                        bot_utils.calc_time_since_last_tweet(datetime.strptime(row[0]
                            , "%Y-%m-%d_%H:%M:%S"), dt_tweet))
                    print(str(datetime.now()) + ' -- ' + 'Time since last type 1 tweet for this route and destNm: ' + str(time_since_last_tweet) + ' minutes')
                    # If it's been longer than 60 minutes since the last tweet about this, tweet it again
                    if time_since_last_tweet >= type_1_wait or n_rows < 1:
                        tweet_type = 1





            # This code looks for tweets every cycle (every sleep_length_record)

            print('\n')
            if tweet_type >= 0:
                print(str(datetime.now()) + ' -- ' + 'Tweeting: ' + potential_tweets[tweet_type])
                print('Has length of: ' + str(len(potential_tweets[tweet_type] + ' #CTA')))
                if post_tweet: 
                    bot_utils.post_tweet(potential_tweets[tweet_type])
                cur.execute(str(potential_tweets_data[tweet_type].table_fill_command()))
            con.commit()

            if tweet_type < 0:
                print(str(datetime.now()) + ' -- ' + 'No suitable tweets found, sleeping until next cycle.' )
            else:
                print(str(datetime.now()) + ' -- ' + 'Sleeping until next cycle.')



            sleep_counter = 0
            while sleep_counter < sleep_length_record:
                if self.deactivate == True:
                    break
                time.sleep(1)
                sleep_counter = sleep_counter + 1

            if self.deactivate == True:
                break

            counter+=1








class MyStreamListener(tweepy.StreamListener):

    def __init__(self, corpus, api, train_statuses):
        self.corpus = corpus
        self.api = api
        self.train_statuses = train_statuses

    def on_error(self, status):
        print(str(datetime.now()) + ' -- ' + 'Stream Error')
        print(status)
        self.running = False
        return
    
    def on_timeout(self):
        print(str(datetime.now()) + ' -- ' + 'Stream Timeout')
        self.running = False
        return
        
    def on_disconnect(self, notice):
        print(str(datetime.now()) + ' -- ' + 'Stream Disconnect')
        print(notice)
        self.running = False
        return

    def on_status(self, status):
        tweet = status.text
        user_reply = status.user.screen_name
        print('\n')
        print(str(datetime.now()) + ' -- ' + "MyStreamListener")

        # Record the time when a tweet might be made
        dt_tweet = datetime.now(pytz.timezone('US/Central'))


        # Start by looking if a line and destination were named
        train_line = ''
        destination = ''
        found_train_line = False
        found_destination = False
        for train_statuses_key, train_statuses_value in self.train_statuses.items():
            if train_statuses_key in tweet:
                train_line = train_statuses_key
                found_train_line = True

        if found_train_line:
            for destinations_key, destinations_value  in self.train_statuses[train_line].destinations.items():
                if destinations_key in tweet:
                    destination = destinations_key
                    found_destination = True

        if found_destination and found_train_line:
            train_delay_tweet = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==1].index,1)].iloc[0][1].replace("[destNm]",destination).replace('[route_name]',train_line).replace("[delay_minutes]",str(int(self.train_statuses[train_line].get_average_delay(destination))))
            if self.train_statuses[train_line].get_average_delay(destination) > 1:
                train_delay_tweet = train_delay_tweet.replace("[minute_s]","minutes")
            elif self.train_statuses[train_line].get_average_delay(destination) == 1:
                train_delay_tweet = train_delay_tweet.replace("[minute_s]","minute")
            elif self.train_statuses[train_line].get_average_delay(destination) < 1:
                train_delay_tweet = 'No delays found on the ' + train_line + ' heading towards ' + destination 

            print(str(datetime.now()) + ' -- ' + '@' + user_reply + ' ' + train_delay_tweet)
            bot_utils.post_tweet('@' + user_reply + ' ' + train_delay_tweet)
            return




        # Look for mentions of Cubs or Sox
        baseball_games = baseball.find_baseball_games()
        found_sox = False
        found_cubs = False
        found_sox_started = False
        found_cubs_started = False
        baseball_tweet_sox = ''
        baseball_tweet_cubs = ''
        # Start by looking for Cubs
        if 'Cubs' in tweet:
            for baseball_game in baseball_games:
                if baseball_game.datetime > dt_tweet.replace(tzinfo=None):
                    if baseball_game.team == 'cubs':
                        baseball_tweet_cubs = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==2].index,1)].iloc[0][1].replace("[team_name]","Cubs").replace("[stadium]","Wrigley Field").replace("[stop_name]","Addison (Red)").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                        found_cubs = True
                else:
                    if baseball_game.team == 'cubs':
                        baseball_tweet_cubs = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==3].index,1)].iloc[0][1].replace("[team_name]","Cubs").replace("[stadium]","Wrigley Field").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                        found_cubs_started = True

            if found_cubs or found_cubs_started:
                print(str(datetime.now()) + ' -- ' + '@' + user_reply + ' ' + baseball_tweet_cubs)
                bot_utils.post_tweet('@' + user_reply + ' ' + baseball_tweet_cubs)
                return
            else:
                print(str(datetime.now()) + ' -- ' + '@' + user_reply + ' ' + 'No Cubs game today.')
                bot_utils.post_tweet('@' + user_reply + ' ' + 'No Cubs game today.')
                return
        # Continue by looking for Sox
        if 'Sox' in tweet:
            for baseball_game in baseball_games:
                if baseball_game.datetime > dt_tweet.replace(tzinfo=None):
                    if baseball_game.team == 'sox':
                        baseball_tweet_sox = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==2].index,1)].iloc[0][1].replace("[team_name]","Sox").replace("[stadium]","U.S. Cellular Field").replace("[stop_name]","Sox-35th").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                        found_sox = True
                else:
                    if baseball_game.team == 'sox':
                        baseball_tweet_sox = self.corpus.loc[numpy.random.choice(self.corpus[self.corpus.template_type==3].index,1)].iloc[0][1].replace("[team_name]","sox").replace("[stadium]","U.S. Cellular Field").replace("[start_time]",baseball_game.datetime.strftime('%I:%M %p'))
                        found_sox_started = True

            if found_sox or found_sox_started:
                print(str(datetime.now()) + ' -- ' + '@' + user_reply + ' ' + baseball_tweet_sox)
                bot_utils.post_tweet('@' + user_reply + ' ' + baseball_tweet_sox)
                return
            else:
                print(str(datetime.now()) + ' -- ' + '@' + user_reply + ' ' + 'No Sox game today.')
                bot_utils.post_tweet('@' + user_reply + ' ' + 'No Sox game today.')
                return



        # Check if status was in tweet
        if 'status' in tweet:
            n_normal_status = 0
            for train_statuses_key, train_statuses_value in self.train_statuses.items():

                # Check if all lines have normal status
                if "Normal Service" in train_statuses_value.route_status:
                    n_normal_status = n_normal_status + 1

            if n_normal_status == 8:
                status_tweet = 'All train lines are operating with Normal Service.'
                print(status_tweet)
                bot_utils.post_tweet('@' + user_reply + ' ' + status_tweet)

        # Say you're welcome if thanked
        if 'thank' in tweet or 'Thank' in tweet:
            bot_utils.post_tweet('@' + user_reply + ' you\'re welcome!')
            return

        if 'Sleep.' in tweet:
            return

        bot_utils.post_tweet('@' + user_reply + ' Sorry, I didn\'t understand that.')
        return











