
import sys
from datetime import datetime, date, time
import time
import cta_status
import bot_utils
import cta_tweet_writer
import pandas
import pytz
import sklearn
import tweepy
import threading
from keys import twitter_keys



def main(argv):


    tweet_type = -1 
    xml_directory = ''
    post_tweet = False
    record_data = False
    stream_to_respond = False

    # Parse arguments
    args = argv[1:]
    while len(args) > 0:
        if args[0] == '-t' or args[0] == '--tweet-type':
            if len(args) > 1:
                tweet_type = int(args[1])
                del args[0:2]
        elif args[0] == '-x' or args[0] == '--xml-directory':
            if len(args) > 1:
                xml_directory = args[1]
                del args[0:2]
        elif args[0] == '-p' or args[0] == '--post-tweet':
            if len(args) > 1:
                if str(args[1]) == 'True':
                    post_tweet = True
                del args[0:2]
        elif args[0] == '-r' or args[0] == '--record-data':
            if len(args) > 1:
                if str(args[1]) == 'True':
                    record_data = True
                del args[0:2]
        elif args[0] == '-s' or args[0] == '--stream_to_respond':
            if len(args) > 1:
                if str(args[1]) == 'True':
                    stream_to_respond = True
                del args[0:2]



    if post_tweet == False:
        print('Not posting the tweet. This is default. Specify --post-tweet True to post.')
    elif post_tweet == True:
        print('Will post the tweet.')

    if post_tweet == False:
        print('Not recording data. This is default. Specify --record-data True to post.')
    elif post_tweet == True:
        print('Will record data.')

    if tweet_type < 0:
        print('No tweet-type specified. Selecting automatically.')

    # Read in the corpus
    corpus = pandas.read_table("corpus.data", delim_whitespace = True)

    # Dictionaries to hold statuses for each line
    # global train_statuses
    blue_status = bot_utils.train_line_status('Blue')
    red_status  = bot_utils.train_line_status('Red')
    green_status  = bot_utils.train_line_status('Green')
    brown_status  = bot_utils.train_line_status('Brown')
    pink_status  = bot_utils.train_line_status('Pink')
    yellow_status  = bot_utils.train_line_status('Yellow')
    orange_status  = bot_utils.train_line_status('Orange')
    purple_status  = bot_utils.train_line_status('Purple')
    train_statuses = dict([('Blue',blue_status),
            ('Red',red_status),
            ('Green',green_status),
            ('Brown',brown_status),
            ('Pink',pink_status),
            ('Yellow',yellow_status),
            ('Orange',orange_status),
            ('Purple',purple_status)])





    # This starts the stream which allows response to tweets
    consumer_key = twitter_keys['consumer_key']
    consumer_secret = twitter_keys['consumer_secret']
    access_token = twitter_keys['access_token']
    access_token_secret = twitter_keys['access_token_secret']

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth)
    myStreamListener = cta_tweet_writer.MyStreamListener(corpus,api,train_statuses )
    if stream_to_respond:
        myStream = tweepy.Stream(auth = api.auth, listener=myStreamListener)
        myStream.filter(track=['L_Tron_CTA'],async=True)

    periodic_tweets = cta_tweet_writer.PeriodicTweets(train_statuses,corpus)

    PeriodicTweetsThread = threading.Thread(target=periodic_tweets.run, args=(xml_directory, record_data, post_tweet,))
    PeriodicTweetsThread.start()


    # If myStream disconnects, try reconnecting 5 times
    # After those 5 times, kill the PeriodicTweetsThread and exit
    stream_reconnect_attempts = 0
    while PeriodicTweetsThread.is_alive():
        if stream_to_respond and myStream.running == False:
            stream_reconnect_attempts+=1
            if stream_reconnect_attempts > 5:
                print(str(datetime.now()) + ' -- ' + "Too many attempts to reconnect to Twitter for streaming, exiting program.")
                periodic_tweets.deactivate()
                break
            else: 
                print(str(datetime.now()) + ' -- ' + "Streaming disconnected, trying to reconnect. This is attempt "+str(stream_reconnect_attempts)+" of 5.")
                myStream.filter(track=['L_Tron_CTA'],async=True)
        time.sleep(1)

    print('PeriodicTweetsThread thread has ended, exiting.')

    # Disconnect the Stream
    # When the PeriodicTweetsThread thread stops, the streaming thread also needs
    # to be killed. This is done by sending "Sleep." to the bot to trigger the on_status 
    # command which will check if disconnect was previously given and actually do the disconnect. 
    if stream_to_respond:
        myStream.disconnect()
        status = bot_utils.post_tweet('@L_Tron_CTA Sleep.')
        api.destroy_status(status.id)
















if __name__ == '__main__':
    sys.exit(main(sys.argv))

