
import bot_utils
import sqlite3
from datetime import datetime, date, time, tzinfo
import pytz
import time
import calendar


def find_baseball_games():

    games = []

    # Our date and time
    dt = datetime.now(pytz.timezone('US/Central'))
    # dt = datetime.strptime("06/17/16 16:30", "%m/%d/%y %H:%M")

    # Connect to the database to get baseball schedule
    con = sqlite3.connect('baseball.db')
    cur = con.cursor()   
    # Find Sox games first
    # Select the date from the schedule if it exists
    date_query = "SELECT * FROM " + "sox" + "_schedule_2016 WHERE `START DATE` = \""
    date_query += dt.strftime('%m') + "/"
    date_query += dt.strftime('%d') + "/"
    date_query += dt.strftime('%y') 
    date_query += "\""


    cur.execute(date_query)
    rows = cur.fetchall()
    for row in rows:
        dt_sox = datetime.strptime(row[1] + ' '+ row[2], "%m/%d/%y %I:%M %p")
        games.append(bot_utils.baseball_game('sox',dt_sox,'U.S. Cellular Field'))


    # Find Cubs games 
    # Select the date from the schedule if it exists
    date_query = "SELECT * FROM " + "cubs" + "_schedule_2016 WHERE `START DATE` = \""
    date_query += dt.strftime('%m') + "/"
    date_query += dt.strftime('%d') + "/"
    date_query += dt.strftime('%y') 
    date_query += "\""


    cur.execute(date_query)
    rows = cur.fetchall()
    for row in rows:
        dt_cubs = datetime.strptime(row[1] + ' '+ row[2], "%m/%d/%y %I:%M %p")
        games.append(bot_utils.baseball_game('cubs',dt_cubs,'Wrigley Field'))

    return games


def find_baseball_game(team, dt = datetime.now(pytz.timezone('US/Central'))):

    # Our date and time
    # dt = datetime.strptime("06/17/15 16:30", "%m/%d/%y %H:%M")

    # Connect to the database to get baseball schedule
    con = sqlite3.connect('baseball.db')
    cur = con.cursor()
    
    # Select the date from the 2016 schedule if it exists
    date_query = "SELECT * FROM " + team + "_schedule_2016 WHERE `START DATE` = \""
    date_query += dt.strftime('%m') + "/"
    date_query += dt.strftime('%d') + "/"
    date_query += dt.strftime('%y') 
    date_query += "\""
    cur.execute(date_query)
    rows = cur.fetchall()
    if len(rows) > 0:
        return True

    # Select the date from the 2015 schedule if it exists
    if dt.strftime('%Y') == '2015':
        date_query = "SELECT * FROM " + team + "_schedule_2015 WHERE `Date` = \""
        date_query += dt.strftime('%A') + " "
        date_query += dt.strftime('%b') + " "
        date_query += dt.strftime('%d') 
        date_query += "\""
        date_query += " AND HA IS NULL"
        cur.execute(date_query)
        rows = cur.fetchall()
        if len(rows) > 0:
            return True


    # Select the date from the 2014 schedule if it exists
    if dt.strftime('%Y') == '2014':
        date_query = "SELECT * FROM " + team + "_schedule_2014 WHERE `Date` = \""
        date_query += dt.strftime('%A') + " "
        date_query += dt.strftime('%b') + " "
        date_query += dt.strftime('%d') 
        date_query += "\""
        date_query += " AND HA IS NULL"
        cur.execute(date_query)
        rows = cur.fetchall()
        if len(rows) > 0:
            return True

    # Select the date from the 2013 schedule if it exists
    if dt.strftime('%Y') == '2013':
        date_query = "SELECT * FROM " + team + "_schedule_2013 WHERE `Date` = \""
        date_query += dt.strftime('%A') + " "
        date_query += dt.strftime('%b') + " "
        date_query += dt.strftime('%d') 
        date_query += "\""
        date_query += " AND HA IS NULL"
        cur.execute(date_query)
        rows = cur.fetchall()
        if len(rows) > 0:
            return True

    # Select the date from the 2012 schedule if it exists
    if dt.strftime('%Y') == '2012':
        date_query = "SELECT * FROM " + team + "_schedule_2012 WHERE `Date` = \""
        date_query += dt.strftime('%A') + " "
        date_query += dt.strftime('%b') + " "
        date_query += dt.strftime('%d') 
        date_query += "\""
        date_query += " AND HA IS NULL"
        cur.execute(date_query)
        rows = cur.fetchall()
        if len(rows) > 0:
            return True

    # Select the date from the 2011 schedule if it exists
    if dt.strftime('%Y') == '2011':
        date_query = "SELECT * FROM " + team + "_schedule_2011 WHERE `Date` = \""
        date_query += dt.strftime('%A') + " "
        date_query += dt.strftime('%b') + " "
        date_query += dt.strftime('%d') 
        date_query += "\""
        date_query += " AND HA IS NULL"
        cur.execute(date_query)
        rows = cur.fetchall()
        if len(rows) > 0:
            return True

    # Look for further Wrigley Field Events 
    date_query = "SELECT * FROM wrigley_events WHERE `START DATE` = \""
    date_query += dt.strftime('%m') + "/"
    date_query += dt.strftime('%d') + "/"
    date_query += dt.strftime('%y') 
    date_query += "\""
    cur.execute(date_query)
    rows = cur.fetchall()
    if len(rows) > 0:
        return True


    return False
