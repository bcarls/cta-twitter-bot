#!/bin/bash

SERVICE='cta-twitter-bot.py'

if ps ax | grep -v grep | grep $SERVICE > /dev/null
then
  echo "$SERVICE service running, all is well!"
else
  echo "$SERVICE is not running"
  echo "CTA twitter bot is not running, restarting it." | mail -s "Starting CTA twitter bot" bcarls@gmail.com
  cd /home/bcarls/cta-twitter-bot

  # Create logile for output
  file_name=bot_logfile.log
  current_time=$(date "+%Y.%m.%d-%H.%M.%S")
  echo "Current Time : $current_time"
  new_fileName=$current_time.$file_name
  echo "Log file name: " "$new_fileName"
  python3 cta-twitter-bot/cta-twitter-bot.py -p True -r False -s True &> $new_fileName
fi
